//premenne: cislo, string, logicka hodnota (true/false); kontajnerove (?); stavove ('atributy')
//funkcie('metody')
//objekt: instancia, ktora kombinuje premenne a funkcie
//trieda: definicia objektu (Zaba)/etiketa - velke pismeno
//instancia: konkretny objekt (zaba Rebeka)

const pocetziab = 10;
const zivotnostzaby = 50;

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

class Zaba {
    //meno;
    constructor(meno) {//konstrukcna funkcia
        this.zivoty = zivotnostzaby;
        this.zije = true;
    }

    ham(pocetmuch) {
        console.log("HAM! ", pocetmuch, " much");
        this.zivoty = this.zivoty - pocetmuch;
        console.log(this.zivoty, " zivotov");
        if (this.zivoty > 0) {
            this.zije = true;
        }
        else {
            this.zije = false;
        }
    }
    poke() {
        //    console.log("Zabka, zijes?");
        if (this.zije === true) {
            console.log("Zabka zije :)")
        }
        else {
            console.log("Zabka nezije :(")
        }
    }

}


//const l = ["a", "b", "c", "d", "e", "f", "g", "h", "ch", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];



let FARMA = [];

for (i = 0; i < pocetziab; i++) {
    const zaba = new Zaba();
    FARMA[i] = zaba;
}

console.log(FARMA);

function krmenie(pocetmuch) {
    for (i = 0; i < pocetziab; i++) { FARMA[i].ham(pocetmuch); }
}
krmenie(15);
console.log(FARMA);
krmenie(25);
console.log(FARMA);

function polokrmenie(kolkoziab, pocetmuch) {
    for (i = 0; i < kolkoziab; i++) { FARMA[i].ham(pocetmuch); }
}

polokrmenie(2, 30);

function GroupPoke() {
    for (i = 0; i < pocetziab; i++) { FARMA[i].poke(); }
}

GroupPoke();

FARMA.forEach(function (zaba) { zaba.ham(7) });
FARMA.forEach(function (zaba) { zaba.poke() });

polokrmenie(4,4);
GroupPoke();

const stavy = FARMA.map(function (zaba) { return (zaba.zije) });
console.log(stavy);