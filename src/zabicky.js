//premenne: cislo, string, logicka hodnota (true/false); kontajnerove (?); stavove ('atributy')
//funkcie('metody')
//objekt: instancia, ktora kombinuje premenne a funkcie
//trieda: definicia objektu (Zaba)/etiketa - velke pismeno
//instancia: konkretny objekt (zaba Rebeka)

class Zaba {
    //meno;
    constructor(meno){//konstrukcna funkcia
        this.meno = meno;
        this.zaludok = 0;//atribut
        this.x = 0;
    }
    ble(pocetmuch){
        console.log(this.meno, "Tfuj...", pocetmuch);
        this.zaludok = this.zaludok - pocetmuch;
    }

    ham(pocetmuch){
        console.log(this.meno, "HAM!", pocetmuch);
        this.zaludok = this.zaludok + pocetmuch;
    }
    hop() {
        console.log("hop", this.meno);//spusti nad touto zabou
        this.x = this.x + 1;
    }
    hip() {
       console.log("hip", this.meno);//spusti nad touto zabou
        this.x = this.x - 1;
    }//metoda
}


const becca = new Zaba("Rebeka")//zrodila sa zaba; Becca = konstruktor = prezyvka; etiketa - male pismeno
console.log(becca);
becca.hop();

const feri = new Zaba("Frantisek")
console.log(feri);
feri.hop();

becca.hop();
feri.hop();

feri.ham(3);
console.log(feri.meno, "zjedol", feri.zaludok);

feri.ble(1);
console.log(feri.meno, "zjedol", feri.zaludok);

becca.hip();

console.log(feri.meno, "poloha: ", feri.x);
console.log(becca.meno, "poloha: ", becca.x);
