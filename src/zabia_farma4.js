//premenne: cislo, string, logicka hodnota (true/false); kontajnerove (?); stavove ('atributy')
//funkcie('metody')
//objekt: instancia, ktora kombinuje premenne a funkcie
//trieda: definicia objektu (Zaba)/etiketa - velke pismeno
//instancia: konkretny objekt (zaba Rebeka)

const pocetziab = 10;
const minzivotnostzaby = 50;

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

//  new Zaba(3)
class Zaba {
    constructor(zivot) {//konstrukcna funkcia
        this.zivoty = zivot;
        this.zije = true;
    }

    ham(pocetmuch) {
        if (this.zivoty < 0) { console.log("mrtve zaby nezeru") }
        else {
            console.log("HAM! ", pocetmuch, " much");
            this.zivoty = this.zivoty - pocetmuch;
            console.log(this.zivoty, " zivotov");
            if (this.zivoty > 0) {
                this.zije = true;
            }
            else {
                this.zije = false;
            }
        }
    }
    poke() {
        //    console.log("Zabka, zijes?");
        if (this.zije === true) {
            console.log("Zabka zije :)")
        }
        else {
            console.log("Zabka nezije :(")
        }
    }

}



let FARMA = [];

for (i = 0; i < pocetziab; i++) {
    let zivotnost = minzivotnostzaby + getRandomInt(20);
    const zaba = new Zaba(zivotnost);
    FARMA[i] = zaba;
}

console.log(FARMA);

function krmenie(pocetmuch) {
    for (i = 0; i < pocetziab; i++) { FARMA[i].ham(pocetmuch); }
}

function polokrmenie(kolkoziab, pocetmuch) {
    for (i = 0; i < kolkoziab; i++) { FARMA[i].ham(pocetmuch); }
}

function GroupPoke() {
    for (i = 0; i < pocetziab; i++) { FARMA[i].poke(); }
}

GroupPoke();

//FARMA.forEach(function (zaba) { zaba.ham(7) });
//FARMA.forEach(function (zaba) { zaba.poke() });

//const stavy = FARMA.map(function (zaba) { return (zaba.zije) });
//console.log(stavy);



function kontrola() {
    //for (i = 0; i < pocetziab; i++) { FARMA[i].poke(); }
    const stavy = FARMA.map(function (zaba) { return (zaba.zije) });
    const zive = stavy.filter(function (stav) { return (stav) }).length;
        console.log("zivych ziab: ",zive);
}

function krmenie() {
    FARMA.forEach(function (zaba) { zaba.ham(6); });
}

for (i = 0; i < 11; i++) {
    krmenie();
    kontrola();
}


//FARMA.forEach(function (zaba) { zaba.ham(48) });

//kontrola();

//while:  do{} while()

//oznac - CtrlShiftR - vonkajsia funkcia