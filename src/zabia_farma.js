//premenne: cislo, string, logicka hodnota (true/false); kontajnerove (?); stavove ('atributy')
//funkcie('metody')
//objekt: instancia, ktora kombinuje premenne a funkcie
//trieda: definicia objektu (Zaba)/etiketa - velke pismeno
//instancia: konkretny objekt (zaba Rebeka)

const pocetziab = 10;
const zivotnostzaby = 50;

class Zaba {
    //meno;
    constructor(meno) {//konstrukcna funkcia
        this.zivoty = zivotnostzaby;
        this.zije = true;
    }

    ham(pocetmuch) {
        console.log ("HAM! ","(", pocetmuch,")");
        this.zivoty = this.zivoty - pocetmuch;
        console.log (this.zivoty, " zivotov");
        if (this.zivoty > 0) {
            this.zije = true;
             }
        else {
            this.zije = false;
             }
        }
    poke() {
        console.log("Zabka, zijes?");
        if (this.zije === true) {
            console.log("Zabka zije :)")
            }
        else  {
            console.log("Zabka nezije :(")
                }
            }

}

const becca = new Zaba("B")
console.log(becca);
becca.poke();
becca.ham(5);
console.log(becca);
becca.poke();
becca.ham(15);
console.log(becca);
becca.poke();
becca.ham(15);
becca.poke();
becca.ham(15);
becca.poke();
