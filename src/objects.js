object = { x: 1, y: 2 };
const key = Object.keys(object);
console.log(object.x);
console.log(object["y"]);
console.log(key);

console.log();
console.log("#1");

for (let i = 0; i < key.length; i++) {
    
    const k = key[i];
    console.log(k, object[k]);
}

console.log();
console.log("#2");

for (const k in object) {
    console.log(k, object[k]);
}